require "rails_helper"

RSpec.describe CoordinatorProfilesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/coordinator_profiles").to route_to("coordinator_profiles#index")
    end

    it "routes to #new" do
      expect(:get => "/coordinator_profiles/new").to route_to("coordinator_profiles#new")
    end

    it "routes to #show" do
      expect(:get => "/coordinator_profiles/1").to route_to("coordinator_profiles#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/coordinator_profiles/1/edit").to route_to("coordinator_profiles#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/coordinator_profiles").to route_to("coordinator_profiles#create")
    end

    it "routes to #update" do
      expect(:put => "/coordinator_profiles/1").to route_to("coordinator_profiles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/coordinator_profiles/1").to route_to("coordinator_profiles#destroy", :id => "1")
    end

  end
end
