require "rails_helper"

RSpec.describe MessageCopiesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/message_copies").to route_to("message_copies#index")
    end

    it "routes to #new" do
      expect(:get => "/message_copies/new").to route_to("message_copies#new")
    end

    it "routes to #show" do
      expect(:get => "/message_copies/1").to route_to("message_copies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/message_copies/1/edit").to route_to("message_copies#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/message_copies").to route_to("message_copies#create")
    end

    it "routes to #update" do
      expect(:put => "/message_copies/1").to route_to("message_copies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/message_copies/1").to route_to("message_copies#destroy", :id => "1")
    end

  end
end
