require "rails_helper"

RSpec.describe AttachementsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/attachements").to route_to("attachements#index")
    end

    it "routes to #new" do
      expect(:get => "/attachements/new").to route_to("attachements#new")
    end

    it "routes to #show" do
      expect(:get => "/attachements/1").to route_to("attachements#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/attachements/1/edit").to route_to("attachements#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/attachements").to route_to("attachements#create")
    end

    it "routes to #update" do
      expect(:put => "/attachements/1").to route_to("attachements#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/attachements/1").to route_to("attachements#destroy", :id => "1")
    end

  end
end
