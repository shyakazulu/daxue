require 'rails_helper'

RSpec.describe MessageCopy, type: :model do
  it "has a valid factory" do
    expect(build(:message_copy)).to be_valid
  end
  context "relationships" do
    it {should belong_to(:message)}
    it {should belong_to(:folder)}
    it {should belong_to(:recipient).class_name("User")}
  end
end
