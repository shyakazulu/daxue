require 'rails_helper'

RSpec.describe Message, type: :model do
  it "has a valid factory" do
    expect(build(:message)).to be_valid
  end

  context "relationships" do
    it {should belong_to (:author)}
    it {should have_many (:message_copies)}
  end

  context "validations" do
    let(:message) {build(:message)}

    it "requires an author_id" do
      expect(:message).to validate_presence_of(:author_id)
    end

    it "requires valid subject" do
      expect(:message).to ensure_length_of(:subject).is_at_most(50)
    end

    it "requires a body" do
      expect(:message).to validate_presence_of(:body)
    end

    it "requires a valid body" do 
    	expect(:message).to ensure_length_of(:body).is_at_most(5000)
    end
  end
end
