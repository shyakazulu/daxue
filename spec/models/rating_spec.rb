require 'rails_helper'
require 'spec_helper'

describe Rating do
	it "has a valid factory" do 
		expect(build(:rating)).to be_valid
	end
   #rating validations tests begins
      context "validations" do
      	#creating a factory instance
      	let(:rating) { build(:rating) }

      	#rating institution_id validation tests
      	it { expect(rating).to validate_presence_of(:institution_id).with_message("The institution_id is missing") }
      	it { expect(rating).to validate_numericality_of(:institution_id).only_integer.with_message("the institution_id can only be an integer")}
        
        #rating user_id validation tests
      	it { expect(rating).to validate_presence_of(:user_id).with_message("The user_id is missing") }
      	it { expect(rating).to validate_numericality_of(:user_id).only_integer.with_message("the user_id can only be an integer")}

      	#rating value validation tests
      	it { expect(rating).to validate_presence_of(:value).with_message("the rating value is missing") }
      	it { expect(rating).to validate_numericality_of(:value)}

     end
   #rating validations tests ends

   #rating associations tests begins
       context "associations" do 
        #creating a factory instance
      	let(:rating) { build(:rating) }
        
        it { expect(rating).to belong_to(:institution) }
        it { expect(rating).to belong_to(:user) }
       end
   #rating associations tests ends
end