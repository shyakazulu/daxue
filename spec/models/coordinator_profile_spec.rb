require 'rails_helper'
require 'spec_helper'

describe CoordinatorProfile do
  
  it "has a valid factory" do 
  	expect(build (:coordinator_profile)).to be_valid
  end
  
  #coordinator_profile validations tests begins

  context "validations" do 
    #creating factory instance
    let(:coordinator_profile) { build(:coordinator_profile) }

  	#coordinator_profile user_id validations tests
    it { expect(coordinator_profile).to validate_presence_of(:user_id).with_message("The user_id is missing") }
    it { expect(coordinator_profile).to validate_numericality_of(:user_id).only_integer.with_message("The user_id can only be an integer") }
    
    #coordinator_profile institution_id validations tests
    it { expect(coordinator_profile).to validate_presence_of(:institution_id).with_message("The institution_id is missing") }
    it { expect(coordinator_profile).to validate_numericality_of(:institution_id).only_integer.with_message("The institution_id can only be an integer") }
    
    #coordinator_profile level validations tests
    it { expect(coordinator_profile).to validate_presence_of(:level).with_message("The institution_id can only be an integer") }
    it { expect(coordinator_profile).to validate_inclusion_of(:level).in_array(['Bachelor', 'Masters', 'PHD'])}

    #coordinator_profile email validations tests
    it { expect(coordinator_profile).to validate_presence_of(:email).with_message("The email is missing") }
    it { expect(coordinator_profile).to validate_length_of(:email).is_at_most(100) }
    it { expect(coordinator_profile).to allow_value("Johndoe@gmail.com").for(:email) }
    it { expect(coordinator_profile).to validate_confirmation_of(:email) }

  end

  #coordinator_profile validations tests ends

  #coordinator_profile associations tests begins

  context "associations" do 
  	#creating factory instance
    let(:coordinator_profile) { build(:coordinator_profile) }

    it { expect(coordinator_profile).to belong_to(:user) }
    it { expect(coordinator_profile).to belong_to(:institution) }
  end
  #coordinator_profile associations tests ends
end
