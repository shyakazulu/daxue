require 'rails_helper'

RSpec.describe User, type: :model do
  it "has a valid factory" do
     expect(build (:user)).to be_valid
  end
  context "relationship" do
    it {should have_many(:ratings)}

    it {should have_many(:notifications)}
    it {should have_many(:folders)}
    it {should have_one(:coordinator_profile)}
    it {should have_one(:client_profile)}
    it {should have_many(:sent_messages)}
    it {should have_many(:recieved_messages)}
    it {should have_many(:application)}
  end

  context "validations" do
     let(:user) {build(:user)}

     it "requires a first_name" do
       expect(user).to validate_presence_of(:first_name)
     end 

     it "requires an email" do
       expect(user).to validate_presence_of(:email)
     end 

     it "requires a last_name" do 
       expect(user).to validate_presence_of(:email)
     end

     it "requires a valid email" do
       expect(user).to ensure_length_of(:email).is_at_most(100)
     end 

     it "requires a unique email" do
       expect(user).to validate_uniqueness_of(:email)
     end 

     it "validates the length of first_name" do
       expect(user).to ensure_length_of(:first_name).is_at_most(100)
     end

     it "validates the length of last_name" do
       expect(user).to ensure_length_of(:last_name).is_at_most(100)
     end
    
  end
end