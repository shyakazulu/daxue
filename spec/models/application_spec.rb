require 'rails_helper'
require 'spec_helper'

RSpec.describe Application, type: :model do

	it "has a valid factory" do
      expect(build(:application, :sent)).to be_valid
	end
	# relationships 
    context "relationships" do
	   it {should have_many(:attachements)}
	   it {should belong_to(:user)}
	   it {should belong_to(:institution)}
	   it {should belong_to(:course)}
    end
    # validations
    context "validations" do 
      let(:application) {build(:application, :sent)}
      
      it "require an user_id"  do
        expect(application).to validate_presence_of(:user_id)
      end

      it "requires a course_id" do
        expect(application).to validate_presence_of(:course_id)
      end 

      it "requires a highest degree of education" do
        expect(application).to validate_presence_of(:hl_degree)
      end

      it "validates the length of hl_degree" do
       expect(application).to ensure_length_of(:hl_degree).is_at_most(100)
      end

      it "requires a date of graduation" do
        expect(application).to validate_presence_of(:hld_date)
      end

      it "requires an address" do
        expect(application).to validate_presence_of(:address)
      end
      
      it "validates the length of address" do
       expect(application).to ensure_length_of(:address).is_at_most(100)
      end

      it "requires a state" do
        expect(application).to validate_presence_of(:state)
      end

      it "requires a valid state" do
       expect(application).to ensure_inclusion_of(:state).in_array(['sent', 'recieved'])
      end

      it "requires an institution_id" do
        expect(application).to validate_presence_of(:institution_id)
      end

    end
end