require 'rails_helper'
require 'spec_helper'

describe Attachement do
  
  it "has a valid factory" do
  expect(build(:attachement)).to be_valid 
  end

  #attachement validations tests begins

  context "validations" do 
  	#creating factory instance
  	let(:attachement) { build(:attachement) }

  	#attachement application_id validations tests
  	it { expect(attachement).to validate_presence_of(:application_id).with_message("The application_id is missing") }
  	it { expect(attachement).to validate_numericality_of(:application_id).only_integer }

  	#attachement content validations tests
  	it { expect(attachement).to validate_presence_of(:content).with_message("The content(the attachement storing path) is missing") }
  	it { expect(attachement).to validate_length_of(:content).is_at_most(255) }

  	#attachement name validations tests
  	it { expect(attachement).to validate_presence_of(:name).with_message("The attachement name is missing") }
  	it { expect(attachement).to validate_length_of(:name).is_at_most(255) }

  end
  #attachement validations tests ends

  #attachement associations tests begins

  context "associations" do 
    #creating factory instance
  	let(:attachement) { build(:attachement) }

  	it { expect(attachement).to belong_to(:application) }
  	
  end
  #attachement associations tests ends
end
