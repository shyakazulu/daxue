require 'rails_helper'

RSpec.describe Folder, type: :model do
  it "has a valid factory" do
     expect(build (:folder)).to be_valid
  end
  
  context "relationships" do
    it {should belong_to(:recipient).class_name("User")}
    it {should have_many(:messages).class_name("MessageCopy")}
  end

  context "validations" do
    let(:folder) {build(:folder)}

    it "requires a recipient_id" do
      expect(folder).to validate_presence_of(:recipient_id)
    end

    it "requires a parent_id" do
      expect(folder).to validate_presence_of(:parent_id)
    end

    it "requires a name" do
      expect(folder).to validate_presence_of(:name)
    end

    it "requires a type" do
      expect(folder).to validate_presence_of(:type)
    end
  end

end
