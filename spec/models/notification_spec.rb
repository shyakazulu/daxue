require 'rails_helper'
require 'spec_helper'

describe Notification do

 it "has a valid factory" do 
 	expect(build (:notification)).to be_valid
 end

#notification validations tests begins 
 context "validations" do
   #creating notification factory instance
    let(:notification) { build(:notification) }
     
     #notification user_recipient_id tests
    it { expect(notification).to validate_presence_of(:recipient_id).with_message("The recipient_id is missing") }
    it { expect(notification).to validate_numericality_of(:recipient_id).only_integer.with_message("the recipient id can only be an integer")}

    #notification body tests
    it { expect(notification).to validate_presence_of(:body).with_message("The notification's body can't be blank") }

    #notification header tests
    it { expect(notification).to validate_presence_of(:header).with_message("A notification must have a header") }
    it { expect(notification).to validate_length_of(:header).is_at_most(255) }
 end
 #notification validations tests ends

 #notification associations tests begins
 context "associations" do 
 	#creating notification factory instance
    let(:notification) { build(:notification) }

    it { expect(notification).to belong_to(:user)}
 end
 #notification associations tests ends

end
