require "rails_helper"

RSpec.describe Institution, type: :model do
  it "has a valid factory" do
    expect(build(:institution)).to be_valid
  end
  context "relationships" do
    it {should have_many(:courses)}
    it {should have_many(:application)}
  end

  context "validations" do
  	let(:institution) {build(:institution)}

    it "requires a name" do
      expect(institution).to validate_presence_of(:name)
    end

    it "requires a valid name" do
      expect(institution).to ensure_length_of(:name).is_at_most(50)
    end

    it "requires a rating" do
      expect(institution).to validate_presence_of(:rating)
    end

    it "requires a valid rating" do
      expect(institution).to validate_numericality_of(:rating)
    end

    it "requires an address" do
      expect(institution).to validate_presence_of(:address)
    end

    it "requires a valid address" do
      expect(institution).to ensure_length_of(:address).is_at_most(100)
    end
    
    it "requires a key" do
      expect(institution).to validate_presence_of(:key)
    end

    it "requires a valid key" do
      expect(institution).to ensure_length_of(:key).is_at_most(50)
    end

  end
end