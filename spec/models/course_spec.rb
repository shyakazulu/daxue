require 'rails_helper'
require 'spec_helper'

describe Course do 
	 it "has a valid factory" do 
	 	expect(build (:course)).to be_valid
	 end

	 #Course Validations tests begins

	 context "validations" do

	 	#creating course factory instance
        let(:course) { build(:course) }
        
        #Course foreign key validations test

        it { expect(course).to validate_presence_of(:institution_id).with_message("foreign key missing") }
        it { expect(course).to validate_numericality_of(:institution_id).only_integer.with_message("the institution id should only be an integer") }
        #Course name validations test
	 	it { expect(course).to validate_presence_of(:name).with_message("A course must have a name")}
	 	it { expect(course).to validate_uniqueness_of(:name).with_message("a course with that name already exist")}
	 	it { expect(course).to validate_length_of(:name).is_at_most(255)}
	 	# course deadline date validations test
	 	it { expect(course).to validate_presence_of(:deadline).with_message("A course must have a deadline date")}

	 	#course language taught in validations test
	 	it { expect(course).to validate_presence_of(:language).with_message("It's important to give the language in which the course will be taught in")}
        it { expect(course).to validate_inclusion_of(:language).in_array(['English','Chinese'])}
        
        #course fee validations test
        it { expect(course).to validate_presence_of(:fee).with_message("A course must have a tuition fee")}

        #course duration period validations test
        it { expect(course).to validate_presence_of(:duration).with_message("A course must have a duration period")}

        #course level validations test
        it { expect(course).to validate_presence_of(:level).with_message("A course should have a level")}
        it { expect(course).to validate_inclusion_of(:level).in_array(['Bachelor','Masters', 'PHD'])}

     end

     #Course Validations tests ends

     #course associations tests begins

     context "associations" do 

     	#creating course factory instance
        let(:course) { build(:course) }

        it { expect(course).to belong_to(:institution) }

     end

     #course associations tests ends
      

end