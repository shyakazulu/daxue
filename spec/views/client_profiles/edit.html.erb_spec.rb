require 'rails_helper'

RSpec.describe "client_profiles/edit", type: :view do
  before(:each) do
    @client_profile = assign(:client_profile, ClientProfile.create!(
      :country => "MyString",
      :cellphone => "MyString",
      :age => ""
    ))
  end

  it "renders the edit client_profile form" do
    render

    assert_select "form[action=?][method=?]", client_profile_path(@client_profile), "post" do

      assert_select "input#client_profile_country[name=?]", "client_profile[country]"

      assert_select "input#client_profile_cellphone[name=?]", "client_profile[cellphone]"

      assert_select "input#client_profile_age[name=?]", "client_profile[age]"
    end
  end
end
