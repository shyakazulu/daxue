require 'rails_helper'

RSpec.describe "client_profiles/index", type: :view do
  before(:each) do
    assign(:client_profiles, [
      ClientProfile.create!(
        :country => "Country",
        :cellphone => "Cellphone",
        :age => ""
      ),
      ClientProfile.create!(
        :country => "Country",
        :cellphone => "Cellphone",
        :age => ""
      )
    ])
  end

  it "renders a list of client_profiles" do
    render
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Cellphone".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
