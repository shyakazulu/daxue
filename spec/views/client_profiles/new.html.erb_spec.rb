require 'rails_helper'

RSpec.describe "client_profiles/new", type: :view do
  before(:each) do
    assign(:client_profile, ClientProfile.new(
      :country => "MyString",
      :cellphone => "MyString",
      :age => ""
    ))
  end

  it "renders new client_profile form" do
    render

    assert_select "form[action=?][method=?]", client_profiles_path, "post" do

      assert_select "input#client_profile_country[name=?]", "client_profile[country]"

      assert_select "input#client_profile_cellphone[name=?]", "client_profile[cellphone]"

      assert_select "input#client_profile_age[name=?]", "client_profile[age]"
    end
  end
end
