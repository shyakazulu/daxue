require 'rails_helper'

RSpec.describe "attachements/show", type: :view do
  before(:each) do
    @attachement = assign(:attachement, Attachement.create!(
      :application_id => 1,
      :content => "",
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Name/)
  end
end
