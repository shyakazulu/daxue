require 'rails_helper'

RSpec.describe "attachements/new", type: :view do
  before(:each) do
    assign(:attachement, Attachement.new(
      :application_id => 1,
      :content => "",
      :name => "MyString"
    ))
  end

  it "renders new attachement form" do
    render

    assert_select "form[action=?][method=?]", attachements_path, "post" do

      assert_select "input#attachement_application_id[name=?]", "attachement[application_id]"

      assert_select "input#attachement_content[name=?]", "attachement[content]"

      assert_select "input#attachement_name[name=?]", "attachement[name]"
    end
  end
end
