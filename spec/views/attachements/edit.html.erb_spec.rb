require 'rails_helper'

RSpec.describe "attachements/edit", type: :view do
  before(:each) do
    @attachement = assign(:attachement, Attachement.create!(
      :application_id => 1,
      :content => "",
      :name => "MyString"
    ))
  end

  it "renders the edit attachement form" do
    render

    assert_select "form[action=?][method=?]", attachement_path(@attachement), "post" do

      assert_select "input#attachement_application_id[name=?]", "attachement[application_id]"

      assert_select "input#attachement_content[name=?]", "attachement[content]"

      assert_select "input#attachement_name[name=?]", "attachement[name]"
    end
  end
end
