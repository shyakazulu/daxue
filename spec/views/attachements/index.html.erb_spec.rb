require 'rails_helper'

RSpec.describe "attachements/index", type: :view do
  before(:each) do
    assign(:attachements, [
      Attachement.create!(
        :application_id => 1,
        :content => "",
        :name => "Name"
      ),
      Attachement.create!(
        :application_id => 1,
        :content => "",
        :name => "Name"
      )
    ])
  end

  it "renders a list of attachements" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
