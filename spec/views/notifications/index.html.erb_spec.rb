require 'rails_helper'

RSpec.describe "notifications/index", type: :view do
  before(:each) do
    assign(:notifications, [
      Notification.create!(
        :recipient_id => 1,
        :type => 2,
        :body => "MyText",
        :header => "Header"
      ),
      Notification.create!(
        :recipient_id => 1,
        :type => 2,
        :body => "MyText",
        :header => "Header"
      )
    ])
  end

  it "renders a list of notifications" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Header".to_s, :count => 2
  end
end
