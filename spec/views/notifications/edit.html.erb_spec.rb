require 'rails_helper'

RSpec.describe "notifications/edit", type: :view do
  before(:each) do
    @notification = assign(:notification, Notification.create!(
      :recipient_id => 1,
      :type => 1,
      :body => "MyText",
      :header => "MyString"
    ))
  end

  it "renders the edit notification form" do
    render

    assert_select "form[action=?][method=?]", notification_path(@notification), "post" do

      assert_select "input#notification_recipient_id[name=?]", "notification[recipient_id]"

      assert_select "input#notification_type[name=?]", "notification[type]"

      assert_select "textarea#notification_body[name=?]", "notification[body]"

      assert_select "input#notification_header[name=?]", "notification[header]"
    end
  end
end
