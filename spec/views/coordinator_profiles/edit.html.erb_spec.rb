require 'rails_helper'

RSpec.describe "coordinator_profiles/edit", type: :view do
  before(:each) do
    @coordinator_profile = assign(:coordinator_profile, CoordinatorProfile.create!(
      :user_id => 1,
      :institution_id => 1,
      :level => 1
    ))
  end

  it "renders the edit coordinator_profile form" do
    render

    assert_select "form[action=?][method=?]", coordinator_profile_path(@coordinator_profile), "post" do

      assert_select "input#coordinator_profile_user_id[name=?]", "coordinator_profile[user_id]"

      assert_select "input#coordinator_profile_institution_id[name=?]", "coordinator_profile[institution_id]"

      assert_select "input#coordinator_profile_level[name=?]", "coordinator_profile[level]"
    end
  end
end
