require 'rails_helper'

RSpec.describe "coordinator_profiles/show", type: :view do
  before(:each) do
    @coordinator_profile = assign(:coordinator_profile, CoordinatorProfile.create!(
      :user_id => 1,
      :institution_id => 2,
      :level => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
