require 'rails_helper'

RSpec.describe "coordinator_profiles/index", type: :view do
  before(:each) do
    assign(:coordinator_profiles, [
      CoordinatorProfile.create!(
        :user_id => 1,
        :institution_id => 2,
        :level => 3
      ),
      CoordinatorProfile.create!(
        :user_id => 1,
        :institution_id => 2,
        :level => 3
      )
    ])
  end

  it "renders a list of coordinator_profiles" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
