require 'rails_helper'

RSpec.describe "message_copies/show", type: :view do
  before(:each) do
    @message_copy = assign(:message_copy, MessageCopy.create!(
      :recipient_id => 1,
      :message_id => 2,
      :folder_id => 3,
      :state => "State"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/State/)
  end
end
