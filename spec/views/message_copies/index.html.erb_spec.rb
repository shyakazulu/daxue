require 'rails_helper'

RSpec.describe "message_copies/index", type: :view do
  before(:each) do
    assign(:message_copies, [
      MessageCopy.create!(
        :recipient_id => 1,
        :message_id => 2,
        :folder_id => 3,
        :state => "State"
      ),
      MessageCopy.create!(
        :recipient_id => 1,
        :message_id => 2,
        :folder_id => 3,
        :state => "State"
      )
    ])
  end

  it "renders a list of message_copies" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
  end
end
