require 'rails_helper'

RSpec.describe "message_copies/edit", type: :view do
  before(:each) do
    @message_copy = assign(:message_copy, MessageCopy.create!(
      :recipient_id => 1,
      :message_id => 1,
      :folder_id => 1,
      :state => "MyString"
    ))
  end

  it "renders the edit message_copy form" do
    render

    assert_select "form[action=?][method=?]", message_copy_path(@message_copy), "post" do

      assert_select "input#message_copy_recipient_id[name=?]", "message_copy[recipient_id]"

      assert_select "input#message_copy_message_id[name=?]", "message_copy[message_id]"

      assert_select "input#message_copy_folder_id[name=?]", "message_copy[folder_id]"

      assert_select "input#message_copy_state[name=?]", "message_copy[state]"
    end
  end
end
