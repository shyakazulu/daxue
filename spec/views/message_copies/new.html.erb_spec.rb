require 'rails_helper'

RSpec.describe "message_copies/new", type: :view do
  before(:each) do
    assign(:message_copy, MessageCopy.new(
      :recipient_id => 1,
      :message_id => 1,
      :folder_id => 1,
      :state => "MyString"
    ))
  end

  it "renders new message_copy form" do
    render

    assert_select "form[action=?][method=?]", message_copies_path, "post" do

      assert_select "input#message_copy_recipient_id[name=?]", "message_copy[recipient_id]"

      assert_select "input#message_copy_message_id[name=?]", "message_copy[message_id]"

      assert_select "input#message_copy_folder_id[name=?]", "message_copy[folder_id]"

      assert_select "input#message_copy_state[name=?]", "message_copy[state]"
    end
  end
end
