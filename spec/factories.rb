#require
#factories for user

FactoryGirl.define do
	factory :user, :aliases => [:author, :recipient ] do
      sequence(:email) {|n| "foo#{n}@example.com"}
      password_digest "secret"
      first_name "John"
	  last_name "Doe"
	end
  # gender traits
	trait :male do
      gender 'male'
	end
	trait :female do
      gender 'female'
	end
    # application factory
    factory :application do
      sequence(:user_id) { |n| n}
      sequence(:course_id) { |n| n}
      hl_degree "high school diploma"
      hld_date Date.new(2015,2,3)
      address "24,montgomery rd,toronto,ontario,CA"
      institution_id 1
    end

  #factories for course

  factory :course do 
    institution_id 1
    name "Software Engineering"
    deadline "4/12/2016"
    language "English"
    fee "21600RMB"
    duration "4years"
    level "Bachelor"
  end

  #factories for notification

  factory :notification do 
   recipient_id 1
   body "You have a new application"
   header "application notification"
  end

  #factories for rating

  factory :rating do
    institution_id 1
    user_id 1
    value 12.5
  end
    # institution factory
    factory :institution do
      name "Nanjing Daxue"
      address "Nanjing lu,Nanjing,Jiangsu,CN"
      rating 8.4
      key "secret"
    end

 #factories for CoordinatorProfile

 factory :coordinator_profile do
  user_id 1
  institution_id 1
  level "Bachelor"
  email "Johndoe@gmail.com"
 end

 #factories for Attachement

 factory :attachement do 
  application_id 1
  content "images/certificate.png"
  name "bachelor diploma"
 end
    # state taits
    trait :sent do
      state 'sent'
    end
    trait :recieved do
      state 'sent'
    end
    trait :accepted do
      state :accepted
    end

    # message factory
    factory :message do
      sequence(:author_id) { |n| n}
      subject "A random subject"
      body "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit odit, veniam, quia officia explicabo "
    end
    # message copy factory
    factory :message_copy do
      sequence(:message_id) { |n| n}
      sequence(:recipient_id) { |n| n}
      sequence(:folder_id) { |n| n}
    end
    # folder factory
    factory :folder do
       sequence(:recipient_id) { |n| n}
       sequence(:parent_id) {|n| n}
       name "inbox"
       sequence(:type) { |n| n}
    end

end

