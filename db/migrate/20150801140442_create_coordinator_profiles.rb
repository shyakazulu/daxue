class CreateCoordinatorProfiles < ActiveRecord::Migration
  def up
    create_table :coordinator_profiles do |t|
      t.integer :user_id
      t.integer :institution_id
      t.string :level
      t.string :email

      t.timestamps null: false
    end
  end
  def down

  	drop_table :coordinator_profiles
  	
  end
end
