class CreateAttachements < ActiveRecord::Migration
  def up
    create_table :attachements do |t|
      t.integer :application_id
      t.string :content
      t.string :name

      t.timestamps null: false
    end
  end
  def down
  	drop_table :attachements
  	
  end
end
