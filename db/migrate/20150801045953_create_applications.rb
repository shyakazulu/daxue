class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.integer :user_id
      t.integer :course_id
      t.string :hl_degree
      t.datetime :hld_date
      t.string :address
      t.string :state
      t.integer :institution_id

      t.timestamps null: false
    end
  end
end
