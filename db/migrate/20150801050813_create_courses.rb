class CreateCourses < ActiveRecord::Migration
  def up
    create_table :courses do |t|
      t.integer :institution_id
      t.string :name
      t.datetime :deadline
      t.string :language
      t.string :fee
      t.string :duration
      t.string :level

      t.timestamps null: false
    end
  end

  def down
    drop_table :courses
  end
end
