class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :address
      t.float :rating
      t.string :key

      t.timestamps null: false
    end
  end
end
