class AddDeletedToMessageCopy < ActiveRecord::Migration
  def change
  	add_column(:message_copies, :deleted, :boolean)
  end
end
