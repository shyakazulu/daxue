class CreateRatings < ActiveRecord::Migration
  def up
    create_table :ratings do |t|
      t.integer :institution_id
      t.integer :user_id
      t.float :value

      t.timestamps null: false
    end
  end

  def down
  	drop_table :ratings
  end
end
