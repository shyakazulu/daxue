class CreateClientProfiles < ActiveRecord::Migration
  def up
    create_table :client_profiles do |t|
      t.string :country
      t.integer :user_id
      t.string :cellphone
      t.integer :age
      t.string :gender
      t.string :nationality

      t.timestamps null: false
    end
  end
  def down
    drop_table :client_profiles
    
  end
end
