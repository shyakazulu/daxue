class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.integer :recipient_id
      t.integer :parent_id
      t.string :name
      t.integer :type

      t.timestamps null: false
    end
  end
end
