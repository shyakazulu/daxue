class CreateNotifications < ActiveRecord::Migration
  def up
    create_table :notifications do |t|
      t.integer :recipient_id
      t.text :body
      t.string :header

      t.timestamps null: false
    end
  end
  def down
  	drop_table :notifications
  end
end
