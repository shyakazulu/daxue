# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150803160623) do

  create_table "admins", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "applications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "hl_degree"
    t.datetime "hld_date"
    t.string   "address"
    t.string   "state"
    t.integer  "institution_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "attachements", force: :cascade do |t|
    t.integer  "application_id"
    t.string   "content"
    t.string   "name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "client_profiles", force: :cascade do |t|
    t.string   "country"
    t.integer  "user_id"
    t.string   "cellphone"
    t.integer  "age"
    t.string   "gender"
    t.string   "nationality"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "gender"
  end

  create_table "coordinator_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "institution_id"
    t.string   "level"
    t.string   "email"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "coordinators", force: :cascade do |t|
    t.integer  "institution_id"
    t.integer  "user_id"
    t.string   "name"
    t.string   "level"
    t.string   "password_digest"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "institution_id"
    t.string   "name"
    t.datetime "deadline"
    t.string   "language"
    t.string   "fee"
    t.string   "duration"
    t.string   "level"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "folders", force: :cascade do |t|
    t.integer  "recipient_id"
    t.integer  "parent_id"
    t.string   "name"
    t.integer  "type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "institutions", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.float    "rating"
    t.string   "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "position"
  end

  create_table "message_copies", force: :cascade do |t|
    t.integer  "recipient_id"
    t.integer  "message_id"
    t.integer  "folder_id"
    t.string   "state"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "deleted"
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "author_id"
    t.string   "subject"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "recipient_id"
    t.text     "body"
    t.string   "header"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "institution_id"
    t.integer  "user_id"
    t.float    "value"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"

end
