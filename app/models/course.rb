class Course < ActiveRecord::Base
	#relationships
	belongs_to :institution
    
    #needed Constants

	LANGUAGE_CHOICES = ['English','Chinese']
	LEVEL_CHOICES = ['Bachelor','Masters', 'PHD']

	#---- validations begins------
    
    # course foreign key validations
    validates :institution_id, :presence => { :message => "foreign key missing"},
                               :numericality => { only_integer: true, :message => "the institution id should only be an integer"}
	# course name validations
    validates :name, :presence => {:message => "A course must have a name"},
                     :uniqueness => {:message => "a course with that name already exist"},
                     :length => {:maximum => 255}

   #course application deadline validations
   validates :deadline, :presence => {:message => "A course must have a deadline date"}

   #validate :check_date, :on => :save

   #course taught in language validations
   validates :language, :presence => {:message => "It's important to give the language in which the course will be taught in"},
                        :inclusion => {:in => LANGUAGE_CHOICES, :message => "the language should be either chinese or english"}

   #course fee validations
   validates :fee, :presence => { :message => "A course must have a tuition fee" }

   #course duration validations
   validates :duration, :presence => { :message => "A course must have a duration period" }

   #course level validations
   validates :level, :presence => { :message => "A course should have a level" },
                     :inclusion => {:in => LEVEL_CHOICES, :message => "Course level is not supported"}

   #---- validations ends------
end
