class Rating < ActiveRecord::Base
	#relationships
	belongs_to :institution
	belongs_to :user

	#validations
    
    #rating institution id validations
    validates :institution_id, :presence => { :message=> "The institution_id is missing"},
                               :numericality => { only_integer: true, :message => "the institution_id can only be an integer"}

    #rating user id validations
    validates :user_id, :presence => { :message => "The user_id is missing"}, 
                        :numericality => { only_integer: true, :message => "the user_id can only be an integer"}                         
    #rating value validations
    validates :value, :presence => { :message => "the rating value is missing"},
                      :numericality => true
end
