class Folder < ActiveRecord::Base
	# act as tree gem instanciation
    acts_as_tree
    # relationships
	belongs_to :recipient, class_name: "User"
	has_many :messages, class_name: "MessageCopy"
    # validations
	validates :recipient_id, presence: true
	validates :parent_id, presence: true
	validates :name, presence: true,
	                 length: {maximum: 50}
	validates :type, presence: true
end
