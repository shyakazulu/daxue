class User < ActiveRecord::Base
	has_many :ratings
	has_many :notifications, foreign_key: "recipient_id"
	has_many :folders, foreign_key: "recipient_id"
	has_many :sent_messages, class_name: "Message", foreign_key: "author_id"
	has_many :recieved_messages, class_name: "MessageCopy", foreign_key: "recipient_id"
	has_many :application
    has_one :coordinator_profile
	has_one  :client_profile
	rolify

	has_secure_password
	before_save :downcase_email
	before_create :build_inbox
    EMAIL_REGEX = /\A(\S+)@(.+)\.(\S+)\z/
    # email validations
    validates :email, presence: true,
	                  uniqueness: true,
	                  length: {within: 4..100},
	                  format: EMAIL_REGEX
    # first_name validations
	validates :first_name, presence: true,
	                       length: {maximum: 100}
	# last_name validations
	validates :last_name,  presence: true,
	                       length: {maximum: 100}
    
	scope :newest_first, lambda { order("users.created_at DESC") }
    
    def downcase_email
      # changes email string to downcase
      self.email = self.email.downcase!
	end
    
    def inbox
      # looks up the inbox folder
      folders.find_by_name("Inbox")
    end
	
	def build_inbox
	  # builds the inbox folder for the user
	  folders.build(name: "Inbox")
	end

	def full_name
	  # concatinates the first_name and the last_name
  	  first_name + " " + last_name
    end

    def add_admin
      # adds the admin role to user's roles
      self.add_role :admin
    end

    def add_client
      # adds the client role to user's roles
      self.add_role :client
    end

    def add_coordinator
      # adds the coordinator role to user's roles
      self.add_role :coordinator
    end

end
