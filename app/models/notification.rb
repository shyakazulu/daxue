class Notification < ActiveRecord::Base
	#relationships
	belongs_to :user, :foreign_key => "recipient_id"
	#validations
     
    #Notification user_recipient_id validations

    validates :recipient_id, :presence => { :message => "The recipient_id is missing"},
                             :numericality => { only_integer: true, message: "the recipient id can only be an integer"}
	#Notification body validations
	validates :body, :presence => { message: "The notification's body can't be blank" }
    
    #Notification header validations                
	validates :header, :presence => { :message => "A notification must have a header"},
	                   :length => {:maximum => 255}


end
