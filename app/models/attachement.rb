class Attachement < ActiveRecord::Base
	#relationships
	belongs_to :application

	#validations begins

	validates :application_id, :presence => { :message => "The application_id is missing" },
	                           :numericality => { only_integer: true }
    
    validates :content, :presence => { :message => "The content(the attachement storing path) is missing" },
                        :length => { :maximum => 255 }

    validates :name, :presence => { :message => "The attachement name is missing" },
                     :length => { :maximum => 255 }
	#validations ends
end
