class Message < ActiveRecord::Base
	# relationship
	belongs_to :author, class_name: "User"
	has_many :message_copies
	has_many :recipients, :through => :message_copies
    
    # callbacks
	before_create :prepare_copies
	after_create :notify
	attr_accessor :to
    
    # validations
    validates :author_id, presence: true
    validates :subject, length: {maximum: 50}
    validates :body, presence: true,
                     length: {maximum: 5000}
    
    # prepares message copy
	def prepare_copies
    	return if to.blank?

    	to.each do |recipient|
          recipient = User.find(recipient)
          message_copies.build(recipient_id: recipient.id, folder_id: recipient.inbox.id)
    	end
    end
    # notify user
    def notify
      return if to.blank?

      to.each do |recipient|
        recipient = User.find(recipient)
        notifier.notify(recipient)
      end
    end
end
