class Institution < ActiveRecord::Base
	# relationships
	has_many :courses
	has_many :application
    
    # sorts Institutions by position
	scope :sorted, lambda { order("institutions.position ASC") }
	scope :newest_first, lambda { order("institutions.created_at DESC") }
    # name validations
	validates :name, presence: true,
	                length: {maximum: 50}
	# rating validations
	validates :rating, presence: true,
	                  numericality: {
	                  	greater_than_or_equal_to: 1,
	                  	less_than_or_equal_to: 10
	                  }
	# address validations
	validates :address, presence: true,
	                length: {maximum: 100}
	# key validations
	validates :key, presence: true,
	                length: {maximum: 50}


end
