class CoordinatorProfile < ActiveRecord::Base
	#relationships
	belongs_to :user
	belongs_to :institution
    
    #needed constants
     LEVEL_CHOICES = ['Bachelor','Masters','PHD']
     EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\Z/i

	#validations begins
    
    validates :user_id, :presence => { :message => "The user_id is missing" },
                       :numericality => { only_integer: true, message: "The user_id can only be an integer" }

     
    validates :institution_id, :presence => { :message => "The institution_id is missing" },
                              :numericality => { only_integer: true, message: "The institution_id can only be an integer" }
    
    validates :level, :presence => { :message => "The institution_id can only be an integer" },
                      :inclusion => { :in => LEVEL_CHOICES, :message => "The level can only be Bachelor,Masters or PHD" }

    validates :email, :presence => { :message => "The email is missing" },
                      :length => { :maximum => 100 },
                      :format => EMAIL_REGEX,
                      :confirmation => true
	#validations ends


end
