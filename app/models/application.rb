class Application < ActiveRecord::Base
	# relationships
	has_many :attachements
	belongs_to :user
	belongs_to :institution
	belongs_to :course

    # user_id validations
	validates :user_id, presence: true
	# course_id validations
	validates :course_id, presence: true
	# hl_degree validations
	validates :hl_degree,presence: true,
	                     length: {maximum: 100}
	# hld_date validations
	validates :hld_date, presence: true
	validate do
      self.errors[:hld_date] << "must be a valid date" unless (DateTime.parse(self.hld_date) rescue false)
    end
    # address validations
	validates :address, presence: true,
	                   length: {maximum: 100}
	# state validations
	validates :state, presence: true,
	                  length: {maximum: 8}
	# institution_id validations
	validates :institution_id, presence: true
    
	scope :sent, lambda {where(:state => "sent")}
	scope :recieved, lambda {where(:state => "recieved")}
	scope :accepted, lambda {where(:state => "accepted")}
	scope :sorted, lambda { order("applications.position ASC") }
	scope :newest_first, lambda { order("applications.created_at DESC") }

end
