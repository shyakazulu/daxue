json.array!(@attachements) do |attachement|
  json.extract! attachement, :id, :application_id, :content, :name
  json.url attachement_url(attachement, format: :json)
end
