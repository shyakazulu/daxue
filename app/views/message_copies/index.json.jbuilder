json.array!(@message_copies) do |message_copy|
  json.extract! message_copy, :id, :recipient_id, :message_id, :folder_id, :state
  json.url message_copy_url(message_copy, format: :json)
end
