json.array!(@client_profiles) do |client_profile|
  json.extract! client_profile, :id, :country, :cellphone, :age
  json.url client_profile_url(client_profile, format: :json)
end
