json.array!(@folders) do |folder|
  json.extract! folder, :id, :recipient_id, :parent_id, :name, :type
  json.url folder_url(folder, format: :json)
end
