json.array!(@notifications) do |notification|
  json.extract! notification, :id, :recipient_id, :type, :body, :header
  json.url notification_url(notification, format: :json)
end
