if User.exists?(@user)
   json.tag "register"
   json.error false
   json.(@user, :id, :first_name, :last_name, :email, :created_at)
else
   json.tag "register"
   json.success 0
   json.error true
   json.error_msg "User already exists"
end

