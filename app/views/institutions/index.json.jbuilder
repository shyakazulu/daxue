json.array!(@institutions) do |institution|
  json.extract! institution, :id, :name, :address, :rating, :key
  json.url institution_url(institution, format: :json)
end
