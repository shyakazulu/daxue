json.array!(@courses) do |course|
  json.extract! course, :id, :institution_id, :name, :deadline, :language, :fee, :duration, :level
  json.url course_url(course, format: :json)
end
