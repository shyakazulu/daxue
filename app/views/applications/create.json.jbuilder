if Application.exists?(@application)
   json.tag "Apply"
   json.error false
   json.(@application, :id, :user_id, :hl_degree, :hld_date, :address, :state, :institution_id)
else
   json.tag "Apply"
   json.success 0
   json.error true
   json.error_msg "Please make sure your application is complete. "
end