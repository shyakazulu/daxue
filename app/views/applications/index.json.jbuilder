json.array!(@applications) do |application|
  json.extract! application, :id, :user_id, :hl_degree, :hld_date, :address, :state, :institution_id
  json.url application_url(application, format: :json)
end
