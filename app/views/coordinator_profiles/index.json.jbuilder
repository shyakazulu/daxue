json.array!(@coordinator_profiles) do |coordinator_profile|
  json.extract! coordinator_profile, :id, :user_id, :institution_id, :level
  json.url coordinator_profile_url(coordinator_profile, format: :json)
end
