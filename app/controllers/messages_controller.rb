class MessagesController < ApplicationController

  # GET /messages/1
  # GET /messages/1.json
  def show
    @message = current_user.recieved_messages.find(params[:id])
  end

  def destroy
    @message = current_user.recieved_messages.find(params[:id])
    @message.update_attributes("deleted", true)
    redirect_to inbox_path
  end

  def reply
    @original = current_user.recieved_messages.find(params[:id])
    subject = @original.subject.sub(/^(Re: )?/, "Re: ")
    body = @original.body.gsub(/^/, "> ")
    @message = current_user.sent_message.build(:to => [@original.author.id], :subject => subject, :body => body)
    render :template => "sent/new" 
  end
  
  def forward
    @original = current_user.recieved_messages.find(params[:id])
    subject = @original.subject.sub(/^(Fwd: )?/, "Fwd:")
    body = @original.body.gsub(/^/, "> ")
    @message = current_user.sent_message.build(subject => subject, :body => body)
    render :template => "sent/new" 
  end
  def reply_all
    @original = current_user.recieved_messages.find(params[:id])
    subject = @original.subject.sub(/^(Re: )?/, "Re: ")
    body = @original.body.gsub(/^/, "> ")
    recipients = @original.recipients.map(&:id) - [current_user.id] + [@original.author.id]
    @message = current_user.sent_message.build(:to => recipients, :subject => subject, :body => body)
    render :template => "sent/new" 
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:author_id, :subject, :body)
    end
end
