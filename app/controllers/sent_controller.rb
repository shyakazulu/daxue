class SentController < ApplicationController
  before_action :require_user
  def index
  	@messages = current_user.sent_messages.paginate :per_page => 10, :page => params[:page], :order => "created_at DESC"
  end

  def show
  	@message = current_user.sent_messages.find(params[:id])
  end

  def new
  	@message = current_user.sent_messages.build(message_params)

  	if message.save
  		flash[:notice] = "Message sent"
  		redirect_to action: "index"
    else
    	render action: "new"
  	end
  end
  private 

  def message_params
    params.require(:message).permit(:author_id, :body, :subject)
  end
end
