class AttachementsController < ApplicationController
  before_action :set_attachement, only: [:show, :edit, :update, :destroy]

  # GET /attachements
  # GET /attachements.json
  def index
    @attachements = Attachement.all
  end

  # GET /attachements/1
  # GET /attachements/1.json
  def show
  end

  # GET /attachements/new
  def new
    @attachement = Attachement.new
  end

  # GET /attachements/1/edit
  def edit
  end

  # POST /attachements
  # POST /attachements.json
  def create
    @attachement = Attachement.new(attachement_params)

    respond_to do |format|
      if @attachement.save
        format.html { redirect_to @attachement, notice: 'Attachement was successfully created.' }
        format.json { render :show, status: :created, location: @attachement }
      else
        format.html { render :new }
        format.json { render json: @attachement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attachements/1
  # PATCH/PUT /attachements/1.json
  def update
    respond_to do |format|
      if @attachement.update(attachement_params)
        format.html { redirect_to @attachement, notice: 'Attachement was successfully updated.' }
        format.json { render :show, status: :ok, location: @attachement }
      else
        format.html { render :edit }
        format.json { render json: @attachement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attachements/1
  # DELETE /attachements/1.json
  def destroy
    @attachement.destroy
    respond_to do |format|
      format.html { redirect_to attachements_url, notice: 'Attachement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attachement
      @attachement = Attachement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attachement_params
      params.require(:attachement).permit(:application_id, :content, :name)
    end
end
