class MailboxController < ApplicationController
  before_action :require_user
  
  def index
  	redirect_to new_session_path and return unless logged_in?
  	@folder = current_user.inbox
    render :action => "show"
  end

  def show
  	@folder ||= current_user.folders.find(params[:id])
  	@message = @folder.messages.paginate :per_page => 10, page => params[:page], include => :message, :order => "message.created_at DESC"
  end

  def trash
  	@folder = Struct.new(:name, :user_id).new("Trash", current_user.id)
  	@message = current_user.recieved_message.paginate_deleted :all, :per_page => 10, :page => params[:page], include => :message, :order => "message.created_at DESC"
  	render :action => "show"
  end
end
