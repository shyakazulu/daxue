class CoordinatorProfilesController < ApplicationController
  before_action :set_coordinator_profile, only: [:show, :edit, :update, :destroy]

  # GET /coordinator_profiles
  # GET /coordinator_profiles.json
  def index
    @coordinator_profiles = CoordinatorProfile.all
  end

  # GET /coordinator_profiles/1
  # GET /coordinator_profiles/1.json
  def show
  end

  # GET /coordinator_profiles/new
  def new
    @coordinator_profile = CoordinatorProfile.new
  end

  # GET /coordinator_profiles/1/edit
  def edit
  end

  # POST /coordinator_profiles
  # POST /coordinator_profiles.json
  def create
    @coordinator_profile = CoordinatorProfile.new(coordinator_profile_params)

    respond_to do |format|
      if @coordinator_profile.save
        format.html { redirect_to @coordinator_profile, notice: 'Coordinator profile was successfully created.' }
        format.json { render :show, status: :created, location: @coordinator_profile }
      else
        format.html { render :new }
        format.json { render json: @coordinator_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coordinator_profiles/1
  # PATCH/PUT /coordinator_profiles/1.json
  def update
    respond_to do |format|
      if @coordinator_profile.update(coordinator_profile_params)
        format.html { redirect_to @coordinator_profile, notice: 'Coordinator profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @coordinator_profile }
      else
        format.html { render :edit }
        format.json { render json: @coordinator_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coordinator_profiles/1
  # DELETE /coordinator_profiles/1.json
  def destroy
    @coordinator_profile.destroy
    respond_to do |format|
      format.html { redirect_to coordinator_profiles_url, notice: 'Coordinator profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coordinator_profile
      @coordinator_profile = CoordinatorProfile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coordinator_profile_params
      params.require(:coordinator_profile).permit(:user_id, :institution_id, :level)
    end
end
