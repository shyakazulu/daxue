class MessageCopiesController < ApplicationController
  before_action :set_message_copy, only: [:show, :edit, :update, :destroy]

  # GET /message_copies
  # GET /message_copies.json
  def index
    @message_copies = MessageCopy.all
  end

  # GET /message_copies/1
  # GET /message_copies/1.json
  def show
  end

  # GET /message_copies/new
  def new
    @message_copy = MessageCopy.new
  end

  # GET /message_copies/1/edit
  def edit
  end

  # POST /message_copies
  # POST /message_copies.json
  def create
    @message_copy = MessageCopy.new(message_copy_params)

    respond_to do |format|
      if @message_copy.save
        format.html { redirect_to @message_copy, notice: 'Message copy was successfully created.' }
        format.json { render :show, status: :created, location: @message_copy }
      else
        format.html { render :new }
        format.json { render json: @message_copy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /message_copies/1
  # PATCH/PUT /message_copies/1.json
  def update
    respond_to do |format|
      if @message_copy.update(message_copy_params)
        format.html { redirect_to @message_copy, notice: 'Message copy was successfully updated.' }
        format.json { render :show, status: :ok, location: @message_copy }
      else
        format.html { render :edit }
        format.json { render json: @message_copy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /message_copies/1
  # DELETE /message_copies/1.json
  def destroy
    @message_copy.destroy
    respond_to do |format|
      format.html { redirect_to message_copies_url, notice: 'Message copy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message_copy
      @message_copy = MessageCopy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_copy_params
      params.require(:message_copy).permit(:recipient_id, :message_id, :folder_id, :state)
    end
end
